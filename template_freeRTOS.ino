/*
 * Example to demonstrate thread definition, semaphores, and thread sleep.
 */
 
#include <Arduino_FreeRTOS.h>
#include <croutine.h>
#include <event_groups.h>
#include <FreeRTOSConfig.h>
#include <FreeRTOSVariant.h>
#include <list.h>
#include <mpu_wrappers.h>
#include <portable.h>
#include <portmacro.h>
#include <projdefs.h>
#include <queue.h>
#include <semphr.h>
#include <StackMacros.h>
#include <task.h>
#include <timers.h>
#include <TimerOne.h>


#define TIMER_US    50
#define INTERRUPT    0            // Interrupt id for pin 2
#define LED_PIN     13            // The LED is attached to pin 13 on Arduino.           

// Declare a semaphore handle.
SemaphoreHandle_t sem;

static void Task1(void* arg)
{
  pinMode(LED_PIN, OUTPUT);

  while (1)
  {
    // Turn LED on.
    digitalWrite(LED_PIN, HIGH);

    // Sleep for 200 milliseconds.
    vTaskDelay((200L * configTICK_RATE_HZ) / 1000L);

    // Signal thread 1 to turn LED off.
    digitalWrite(LED_PIN, LOW);
    // Sleep for 200 milliseconds.
    vTaskDelay((200L * configTICK_RATE_HZ) / 1000L);
  }
}

/*** Timer ISR ***/
void TimerISR()     //Timer interrupt every 50us
{
  static int i = 0;
  
  if(i == 400) // cada 20ms mando el trigger de 50us
  {

    i = 0;
  }
  
  i++;
}

/*** Hardware ISR ***/
void IRQHandler()
{
  // Hardware interrupt code
}

void HardwareInit()
{
  adcInit();
  Timer1.initialize(TIMER_US);                        // Initialise timer 1
  Timer1.attachInterrupt(TimerISR);                 // Attach interrupt to the timer service routine 
  
  attachInterrupt(INTERRUPT, IRQHandler, CHANGE);  // Attach interrupt to the sensor echo input
  
  Serial.begin(9600);
}

void adcInit()
{
  // Define various ADC prescaler
  const unsigned char PS_16 = (1 << ADPS2);
  const unsigned char PS_32 = (1 << ADPS2) | (1 << ADPS0);
  const unsigned char PS_64 = (1 << ADPS2) | (1 << ADPS1);
  const unsigned char PS_128 = (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);

  // set up the ADC
  ADCSRA &= ~PS_128;  // remove bits set by Arduino library

  // you can choose a prescaler from above.
  // PS_16, PS_32, PS_64 or PS_128
  ADCSRA |= PS_128;    // set our own prescaler to 128 
}

//------------------------------------------------------------------------------
void setup()
{

  HardwareInit();
  
  vSemaphoreCreateBinary(sem);

  xTaskCreate(Task1, NULL, configMINIMAL_STACK_SIZE, NULL, 2, NULL);

  //xTaskCreate(Thread2, NULL, configMINIMAL_STACK_SIZE, NULL, 1, NULL);

  vTaskStartScheduler();
  Serial.println(F("Insufficient RAM"));
  while(1);
}
//------------------------------------------------------------------------------
// WARNING idle loop has a very small stack (configMINIMAL_STACK_SIZE)
// loop must never block
void loop() {
  // Not used.
}