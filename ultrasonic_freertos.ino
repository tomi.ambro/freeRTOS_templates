//#include <basic_io_avr.h>
//#include <FreeRTOS_AVR.h>

#include <Arduino_FreeRTOS.h>
#include <croutine.h>
#include <event_groups.h>
#include <FreeRTOSConfig.h>
#include <FreeRTOSVariant.h>
#include <list.h>
#include <mpu_wrappers.h>
#include <portable.h>
#include <portmacro.h>
#include <projdefs.h>
#include <queue.h>
#include <semphr.h>
#include <StackMacros.h>
#include <task.h>
#include <timers.h>
#include <TimerOne.h>
//#include "aux_functions.h"

#include <TimerOne.h>                                 // Header file for TimerOne library

#define trigPin     3                                    // Pin 3 trigger output
#define echoPin     2                                     // Pin 2 Echo input
#define LED         4                                 // Pin 13 onboard LED
#define BOARD_LED   13
#define echo_int    0                                    // Interrupt id for echo pulse
#define BUFFER_SIZE 20
#define TIMER_US    50

SemaphoreHandle_t signal;
SemaphoreHandle_t led;
//QueueHandle_t queue;
volatile long echo_start = 0;                         // Records start of echo pulse 
volatile long echo_end = 0;                           // Records end of echo pulse
volatile long echo_duration = 0;                      // Duration - difference between end and start
volatile long timer = 0;
volatile float buffer[BUFFER_SIZE];
volatile float distance = 0;

void setup() 
{
  pinMode(trigPin, OUTPUT);                           // Trigger pin set to output
  pinMode(echoPin, INPUT);                            // Echo pin set to input
  pinMode(LED, OUTPUT);                        // Onboard LED pin set to output
  pinMode(BOARD_LED, OUTPUT);

  Timer1.initialize(TIMER_US);                        // Initialise timer 1
  Timer1.attachInterrupt( timerIsr );                 // Attach interrupt to the timer service routine 
  attachInterrupt(echo_int, echo_interrupt, CHANGE);  // Attach interrupt to the sensor echo input
  Serial.begin(9600);                                // Initialise the serial monitor output
    
  vSemaphoreCreateBinary(signal);
  vSemaphoreCreateBinary(led);
  
  xTaskCreate(filter, "main loop", configMINIMAL_STACK_SIZE*2, NULL, 2, NULL);
  xTaskCreate(flash_led, "toggle led", configMINIMAL_STACK_SIZE*2, NULL, 1, NULL);
  
  vTaskStartScheduler();
  
  Serial.println("No RAM");
  while(1);
}

void filter(void *pvParameters)
{
  int state;
  
  while(1)
  {
    if(xSemaphoreTake(signal, portMAX_DELAY))
    {
      moving_average();
      //Serial.println(distance);               // Print the distance in centimeters (noise filtered)   
      //Serial.print(" - ");
      //Serial.println((float)echo_duration / 58);          // Print no filter
    }
    vTaskDelay(25);
  }
}

void flash_led(void *param)
{
  //timer = echo_duration / 2;
  timer = 0;
  static int n = 0 ;
  while(1)
  {
    if(distance < 15)       // distancia critica
      digitalWrite(BOARD_LED, HIGH);
    else
      digitalWrite(BOARD_LED, LOW);

    if(distance < 20)       // distancia prudente
      digitalWrite(LED, HIGH);
    else
      digitalWrite(LED, digitalRead(LED) ^ 1 );
    
    //Serial.println(timer);

    vTaskDelay(timer * timer / 1000 / 32); // led toggle proporcional al cuadrado de medio periodo
  }
}

/*** Timer ISR ***/
void timerIsr()
{
  static int i = 0;
  
  digitalWrite(trigPin, LOW);
  
  if(i == 400)
  {
    digitalWrite(trigPin, HIGH);
    i = 0;
  }
  
  i++;
}

/*** Hardware ISR ***/
void echo_interrupt()
{
  switch (digitalRead(echoPin))                     // Test to see if the signal is high or low
  {
    case HIGH:                                      // High so must be the start of the echo pulse
      echo_end = 0;                                 // Clear the end time
      echo_start = micros();                        // Save the start time
      break;
      
    case LOW:                                       // Low so must be the end of hte echo pulse
      echo_end = micros();                          // Save the end time
      echo_duration = echo_end - echo_start;        // Calculate the pulse duration
      break;
  }
  xSemaphoreGiveFromISR(signal, NULL); // mando señal para que se ejecute la tarea que calcula y muestra la distancia
}
/*** Main loop ***/
void loop()
{
}

/*** Aux function ***/
void moving_average()
{
    static int i = 0;
    buffer[i] = echo_duration;
    distance = 0;
    
    for(int n = 0; n < BUFFER_SIZE; ++n)
    {
      distance = distance + buffer[n];
    }
    
    timer = distance / BUFFER_SIZE;

    distance = distance / BUFFER_SIZE  / 58;

    i++;

    if(i == BUFFER_SIZE)
    {
      i = 0; 
    }
}
